import javax.swing.JOptionPane;
public class MenuPrincipal
{
  public static void main (String arg[])
  {
  char opcion='a';
  String datoLeido="";
  
  
  
  
	Computadora computadora=null;
    while (opcion!='h')
    {
    datoLeido=JOptionPane.showInputDialog("Menu principal\n"+
                                                         "a. Reiniciar los valores\n"+
                                                         "b. Registrar la computadora\n"+
                                                         "c. Modificar placa de computadora\n"+
                                                         "d. Modificar modelo de computadora\n"+
                                                         "e. Modificar tamano de pantalla de computadora\n"+
                                                         "f. Modificar el estado de la computadora\n"+
                                                         "g. Ver los datos de la computadora\n"+
                                                         "h  Salir");
  opcion=datoLeido.charAt(0);
  switch (opcion)
    {
      case 'a': //Reinicia los valores de la computadora
        if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        computadora=new Computadora();}
      break;
      
      case 'b'://Registra la computadora con sus valores
      String modelo=JOptionPane.showInputDialog("Digite el modelo de la computadora");
	  String placa=JOptionPane.showInputDialog("Digite la placa de la computadora");
      int pantalla=Integer.parseInt(JOptionPane.showInputDialog("Digite por favor el tamano de la pantalla"));
      computadora= new Computadora (modelo, placa, pantalla);
      JOptionPane.showMessageDialog(null,"Computadora registrada con exito");
      break;
        
      case 'c':
      if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        placa=JOptionPane.showInputDialog("Cual es la nueva placa? Digitela");
		computadora.setPlaca(placa);}
      break;
      
      case 'd':
      if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        modelo=JOptionPane.showInputDialog("Cual es el nuevo modelo");
		computadora.setModelo(modelo);}
      break;
      
      case 'e':
      if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        pantalla=Integer.parseInt(JOptionPane.showInputDialog("Cual es el nuevo tamaño de la pantalla"));
		computadora.setPantalla(pantalla);}
      break;
      
      case 'f':
      if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        String estado=JOptionPane.showInputDialog("Cual es el nuevo estado");
        computadora.setEstado(estado);
		}
      break;
      
      case 'g':
      if (computadora==null)
        {
        JOptionPane.showMessageDialog(null,"Debe registar la computadora primero");
        }
        else{
        JOptionPane.showMessageDialog(null,computadora);
		}
      break;
      
      
      default:
        if (opcion!='h')
      {
      JOptionPane.showMessageDialog(null,"Opción inválida\n"+
                                    "Digite una opción válida por favor");
      }
        break;
    }//Fin del switch
    }
      
} 
}
